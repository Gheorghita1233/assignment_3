
const FIRST_NAME = "Gheorghita";
const LAST_NAME = "Nicolae";
const GRUPA = "1092";

/**
 * Make the implementation here
 */
class Employee {

   constructor(_name, _surname, _salary) {
        this.name = _name;
        this.surname = _surname;
        this.salary=_salary;
    }

    // Adding a method to the constructor
    getDetails () {
        return `${this.name} ${this.surname} ${this.salary}`;
    }

    getSalary() {
        return this.salary;
    }
}

class SoftwareEngineer extends Employee {

    constructor( nume, prenume, salariu) { 
        super(nume,prenume,salariu);
        this.experience="JUNIOR"; 
      }
      
     
      applyBonus ()
      {
          var buffer;
          buffer=this.getSalary();

          if(this.experience=="JUNIOR")
          {
              return buffer*1.1;
          }
          else if(this.experience=="MIDDLE")
          {
              return buffer*1.15;
          }
          else if(this.experience=="SENIOR")
          {
              return buffer*1.2;
          }

       return buffer*1.1;
      }
   
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

